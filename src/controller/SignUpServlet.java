package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import beans.User;
import service.UserService;

// ServletとURLを対応付け
@WebServlet(urlPatterns = { "/signup" })
// HttpServlet = リクエストの受信、レスポンスを返すための受け口
public class SignUpServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
	        throws ServletException, IOException {
		// signup.jspに処理を転送
		request.getRequestDispatcher("signup.jsp").forward(request, response);
	}

	@Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {

		List<String> errorMessages = new ArrayList<String>();

		User user = getUser(request);
		if (!isValid(user, errorMessages)) {
            request.setAttribute("errorMessages", errorMessages);
            request.getRequestDispatcher("signup.jsp").forward(request, response);
            return;
        }
		new UserService().insert(user);
        response.sendRedirect("./");
	}

	private User getUser(HttpServletRequest request)
			throws IOException, ServletException {
		// Userクラスのインスタンス生成
		User user = new User();
		// インスタンスuserを()requestの()と名付ける
		user.setAccount(request.getParameter("account"));
		user.setPassword(request.getParameter("password"));
		user.setRePassword(request.getParameter("rePassword"));
		user.setName(request.getParameter("name"));
		user.setBranchId(Integer.parseInt(request.getParameter("branchId")));
		user.setDepartmentId(Integer.parseInt(request.getParameter("departmentId")));
		return user;
	}

	private boolean isValid(User user, List<String> errorMessages) {

	        String account = user.getAccount();
	        String password = user.getPassword();
	    	String name = user.getName();

	        if (!StringUtils.isEmpty(name) && (20 < name.length())) {
	            errorMessages.add("名前は20文字以下で入力してください");
	        }

	        if (StringUtils.isEmpty(account)) {
	            errorMessages.add("アカウント名を入力してください");
	        } else if (20 < account.length()) {
	            errorMessages.add("アカウント名は20文字以下で入力してください");
	        }

	        if (StringUtils.isEmpty(password)) {
	            errorMessages.add("パスワードを入力してください");
	        }

	        if (errorMessages.size() != 0) {
	            return false;
	        }
	        return true;
	    }
}

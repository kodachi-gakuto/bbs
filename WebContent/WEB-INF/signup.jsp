<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザー登録</title>
</head>
<body>
	<div class="header">
		<a href="/management">ユーザー管理</a>
	</div>

	<div class="main">
		<form action="signup" method="post">
			<p>アカウント</p>
			<input name="account" type="text">

			<p>パスワード</p>
			<input name="password" type="password">

			<p>確認用パスワード</p>
			<input name="rePassword" type="password">

			<p>名前</p>
			<input name="name" type="text">

			<p>支社</p>
			<input name="branchId" type="text">

			<p>部署</p>
			<input name="departmentId" type="text">

			<input name="insert" type="submit" value="登録">
		</form>
	</div>


	<div class="footer">
		<p>Copyright(c) Kodachi Gakuto</p>
	</div>

</body>
</html>
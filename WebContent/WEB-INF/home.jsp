<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored = "false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ホーム</title>
</head>
<body>
	<div class="header">
		<a href="/message">新規投稿</a>
		<a href="/management">ユーザー管理</a>
		<a href="/login">ログアウト</a>
	</div>

	<div class="footer">
		<p>Copyright(c) Kodachi Gakuto</p>
	</div>

</body>
</html>